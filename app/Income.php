<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    public function calendar()
    {
        return $this->belongsTo('App\Calendar');
    }
}
