<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenses extends Model
{
    protected $table = 'expenses';

    protected $fillable = ['date','category_id', 'item', 'details', 'price'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
