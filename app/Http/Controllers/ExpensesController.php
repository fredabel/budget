<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expenses;
use App\Category;
use DB;
use Carbon\Carbon;

class ExpensesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $expenses = Expenses::get();
        $categories = Category::get();
        $expenses = Expenses::with('category')->where(['status'=>'active'])->orderBy('created_at', 'desc')->paginate(5);
        $charts = Expenses::with('category')->where(['status'=>'active'])->groupBy('category_id')->select('category_id', DB::raw('sum(price) as price'))->get();

       return view('content.index', compact(['expenses','charts','categories']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'date'  => 'required',
            'category' => 'required',
            'item' => 'required',
            'details' => 'required',
            'price' => 'required',
    	]);

        /* Add  item */  
        $items= new Expenses();
        $items->date = $request['date'];
        $items->category_id = $request['category'];
        $items->item= ucfirst($request['item']);
        $items->details= ucfirst($request['details']);
        $items->price= $request['price'];
        $items->week = date('W');
        $items->status = 'active';
        $items->save();

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        $item = Expenses::findOrFail($id);
        return view('content.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
