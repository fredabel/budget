<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User; 
class UserController extends Controller
{

    public $successStatus = 200; 

    public function login(){ 
        if(Auth::guard('web')->attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['id']    = $user->id;
            $success['name']    = $user->name;
            $success['email']    = $user->email; 
            $success['avatar']    = $user->avatar;
            $success['msg'] = 'Success';
            return response()->json(['success' => $success], $this->successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], $this->successStatus); 
        } 
    }
    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all(); 
        $input['password'] =  Hash::make($input['password']); 
        $user = User::create($input); 
        $success['token'] =  $user->createToken('MyApp')->accessToken; 
        $success['name'] =  $user->name;
        $success['msg'] = 'Success';
        return response()->json(['success'=> $success], $this->successStatus); 
    }
    /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
    */ 

    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this->successStatus); 
    }

    public function update(Request $request, $id){

        // if($request->hasFile('user-profile')) {
        //     $avatar = $request->file('user-profile');
        //     $filename = time().'.'.$avatar->getClientOriginalExtension();
        //     Image::make($avatar)->resize(300, 300)->save(public_path('/uploads/avatar/'. $filename));
           
        //     $user = User::find($id);
        //     $user->avatar = $filename;
        //     // $user->email = $request->email;
        //     $user->password = bcrypt($request->password);
        //     $user->save();

        // }   

        // return response()->json(['success'=> $request->all()], $this->successStatus); 

        $validator = Validator::make($request->all(), [ 
            'edit-name' => 'required', 
            'edit-email' => 'required|email', 
            'edit-password' => 'required', 
            'c_password' => 'required|same:edit-password', 
        ]);

        if ($validator->fails()) { 

            return response()->json(['error'=> $validator->errors()], 401);         
        }

        $user = User::find($id);
        $user->name = $request['edit-name'];
        $user->email = $request['edit-email'];
        $user->password =  Hash::make($request['edit-password']); 
        $user->save();
       
        return response()->json(['success'=> 'Success'], $this->successStatus); 

    }
    
    public function destroy($id){

        $user = User::find($id);
        $user->status = 'inactive';
        $user->update();

        return response()->json(['success'=> 'Success'], $this->successStatus); 

    }
}
