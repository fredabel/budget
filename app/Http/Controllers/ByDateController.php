<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expenses;
use App\Category;
use App\Calendar;
use DB;
use Carbon\Carbon;

class ByDateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function daily(){

       return view('content.daily');
    }

    public function daily_filter(Request $request){

        $date = $request->daily;

        $data = Expenses::with('category')->where(['date'=> $date,'status'=>'active'])->orderBy('created_at', 'desc')->get();
        $charts = Expenses::with('category')->where(['date'=> $date,'status'=>'active'])->groupBy('category_id')->select('category_id', DB::raw('sum(price) as price'))->get();

        $datas = [$data, $charts];

        return $datas;

    }
    public function weekly(){

        return view('content.weekly');
    }

    public function weekly_filter(Request $request){


        $week = $request->week;
        $year = $request->year;



        $data = Expenses::with('category')->whereYear('date', $year)->where(['week'=> $week,'status'=>'active'])->orderBy('created_at', 'desc')->get();
        $charts = Expenses::with('category')->whereYear('date', $year)->where(['week'=> $week,'status'=>'active'])->groupBy('category_id')->select('category_id', DB::raw('sum(price) as price'))->get();

        $datas = [$data, $charts];

        return $datas;

    }
    public function monthly(){

        return view('content.monthly');
     }

    public function monthly_filter(Request $request){

        $month = $request->month;
        $year = $request->year;



        $data = Expenses::with('category')->whereMonth('date', $request->month)->whereYear('date', $request->year)->where('status','active')->orderBy('created_at', 'desc')->get();
        $charts = Expenses::with('category')->whereMonth('date', $request->month)->whereYear('date', $request->year)->where('status','active')->groupBy('category_id')->select('category_id', DB::raw('sum(price) as price'))->get();


        $datas = [$data, $charts];

        return $datas;

    }
    public function yearly(){

        return view('content.yearly');
    }

    public function yearly_filter(Request $request){



        $expenses = Expenses::whereYear('date', $request->years)->where('status','active')->select(DB::raw('sum(price) as price'),DB::raw( 'MONTHNAME(date) as month'))->groupBy('month')->get();



        return $expenses;


    }


}
