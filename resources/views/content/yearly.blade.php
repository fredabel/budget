@extends('master_layout.master')
@section('yearly')
    @php
        $current_date = Carbon\Carbon::now()->toDateString();
    @endphp
    <div class="row">
        <div class="form-group col-md-2">
            <label for="" class="font-weight-bold">Select Year</label>
            <select name="year" id="years" class="form-control">
                <option value="2019">2019</option>
                <option value="2020">2020</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
            </select>
        </div>
    </div>

    <div id="result"></div>
    <canvas id="mainChart" width="400" height="250"></canvas>

@endsection



@section('script')

    <script>
        $.extend({
            list: function(){
                $.post('/yearly',{_token: $('meta[name=csrf-token]').attr('content'), years:$('#years').val()},function(data){

                    console.log(data);
                    if (data.length == 0) {
                        $("#result").html('No Records Found');
                        $("#result").attr('class','alert alert-info');
                        $("#mainChart").css('display','none');

                    }else{

                        $("#result").removeAttr('class');
                        $("#mainChart").css('display','') ;

                        //chart for sidebar-right

                        var data = data;
                        var price = [];
                        var month = [];
                        var total = 0;
                        var year = $('#years').val();

                        for(var i = 0;  i < data.length; i++) {
                            month.push([data[i]['month']]);
                            price.push([data[i]['price']]);
                            total += data[i]['year_price'] << 0; //convert the string elements to numerics using '<< 0'

                        }

                        var ctx = document.getElementById("mainChart");
                        ctx.width = 500;
                        ctx.height = 300;
                        var myChart = new Chart(ctx, {
                            type: 'line',

                            data: {
                                labels: month,
                                datasets: [{
                                    label: year,
                                    data: price,
                                    borderColor: "#80b6f4",
                                    borderWidth: 2,
                                    fontColor: '#666',
                                    pointBorderWidth: 5,
                                    pointHoverBorderWidth: 1,
                                    fill: true


                                }]
                            },
                            options: {

                                title: {
                                    display: true,
                                    text: years+' Total Expenses: P'+total,
                                    fontSize: 25,
                                    padding: 10


                                },
                                tooltips:{
                                    enable: true,
                                    bodyFontSize: 15,
                                    titleFontSize: 15
                                },
                                legend:{


                                    labels:{
                                        fontSize: 15,
                                        fontColor: '#000',


                                    }
                                },
                                layout: {
                                    padding: {
                                        left: 0,
                                        right: 0,
                                        top: 20,
                                        bottom: 20
                                    }
                                },
                                scales: {
                                    yAxes:
                                    [{
                                        ticks: {
                                            fontColor: "rgba(0,0,0,0.5)",
                                            fontStyle: "bold",
                                            beginAtZero: true,
                                            maxTicksLimit: 10,
                                            padding: 0
                                        },
                                        gridLines: {
                                            drawTicks: false,
                                            display: true
                                        }
                                    }],
                                    xAxes:
                                    [{
                                        gridLines: {
                                            zeroLineColor: "transparent"
                                            },
                                        ticks: {
                                            padding: 3,
                                            fontColor: "rgba(0,0,0,0.5)",
                                            fontStyle: "bold"
                                        }
                                    }]
                                }


                            }

                        });


                    }//else end

               });
            }

        });

    </script>
    <script>
        $.list();
        $('#years').change(function(){
            $.list();
        });

    </script>

@endsection

