@extends('master_layout.master')
@section('content')

<form>
    <div class="form-group">
        <label for="date" class="font-weight-bold text-danger">DATE</label>
        <p class="font-weight-light">{{ $item->date }}</p>
        <hr>
        {{--  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">  --}}
        
    </div>
    <div class="form-group">
        <label for="category" class="font-weight-bold text-danger">CATEGORY</label>
        <p class="font-weight-light">{{ $item->category->title }}</p>
        <hr>
        {{--  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">  --}}
            
    </div>
    <div class="form-group">
        <label for="item" class="font-weight-bold text-danger">ITEM</label>
        <p class="font-weight-light"> {{ $item->item }}</p>
        <hr>
        {{--  <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">  --}}
    </div>
    <div class="form-group">
        <label for="details" class="font-weight-bold text-danger">DETAILS</label>
        <p class="font-weight-light"> {{ $item->details }}</p>
        <hr>
        
    </div>
    <div class="form-group">
        <label for="price" class="font-weight-bold text-danger">PRICE</label>
        <p class="font-weight-light"> P{{ $item->price }}.00</p>
        <hr>    
    </div>
    
    <a href="/expenses" class="btn btn-primary">Back</a>
    {{--  <button type="submit" class="btn btn-primary">Submit</button>  --}}
</form>
@endsection