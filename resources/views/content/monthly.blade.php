@extends('master_layout.master')
@section('content')
    @php
        $current_date = Carbon\Carbon::now()->toDateString();
    @endphp
    <div class="row">
        <div class="form-group col-md-6">
            <label for="" class="font-weight-bold">Month</label>
            <select name="month" id="monthly" class="form-control">
                @php
                    for ($x=1; $x<=12; $x++) {
                        echo '<option value="'.$x.'">'.date('F',mktime(0,0,0,$x,1,date('Y'))).'</option>';
                    }
                @endphp

            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="" class="font-weight-bold">Year</label>
            <select name="year" id="month-year" class="form-control">
                <option value="2019">2019</option>
                <option value="2020">2020</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
            </select>
        </div>
    </div>

    <div id="result">

    </div>

@endsection

@section('sidebar')
    <canvas id="myChart" width="400" height="250"></canvas>
@endsection

@section('script')

    <script>
        $.extend({
            list: function(){
                $.post('/monthly',{_token: $('meta[name=csrf-token]').attr('content'), month:$('#monthly').val(), year:$('#month-year').val()},function(data){

                  
                    if (data[0].length == 0) {
                        $("#result").html('No Records Found');
                        $("#result").attr('class','alert alert-info');
                        $("#myChart").css('display','none');

                    }else{

                        $("#result").removeAttr('class');
                        $("#myChart").css('display','') ;

                        if(data[0].length > 10){
                            $("#result").addClass('scroll');
                        }

                        var content;
                        content += '<tbody>'
                        $.each(data[0], function(i, val){

                            
                            content+='<tr>';
                            content += '<td>'+val.date+'</td>';
                            content += '<td >'+val.category.title+'</td>';
                            content += '<td >'+val.item+'</td>';
                            content += '<td >'+val.details+'</td>';
                            content += '<td >'+val.price+'</td>';
                            content+='</tr>';


                        });
                        content += '</tbody>'
                        

                        var html = '<tr><th>Date</th><th>Category</th><th>Item</th><th>Details</th><th>Price</th>';

                        html+= '</tr>';

                        $('#result').html('<table class="table shadow-lg" ></table>');
                        $(".table").html(html);
                        $(".table").append(content);


                        //chart

                        var data = data[1];
                        var price = [];
                        var title = [];
                        var total = 0;

                        for(var i = 0;  i < data.length; i++) {
                            title.push([data[i]['category']['title']]);
                            price.push([data[i]['price']]);
                            total += data[i]['price'] << 0; //convert the string elements to numerics using '<< 0'

                        }


                        var ctx = document.getElementById("myChart");
                        ctx.width = 500;
                        ctx.height = 400;
                        var myChart = new Chart(ctx, {
                            type: 'pie',
                            data: {
                                labels: title,
                                datasets: [{
                                    data: price,
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(255, 159, 64, 0.2)'
                                    ],

                                    borderWidth: 1,
                                    fontColor: '#666'


                                }]
                            },
                            options: {

                                title: {
                                    display: true,
                                    text: 'Monthly Total Expenses: '+total,
                                    fontSize: 25,
                                    padding: 20


                                },
                                tooltips:{
                                    enable: true,
                                    bodyFontSize: 20
                                },
                                legend:{
                                    display: true,
                                    position: 'right',
                                    labels:{
                                        fontSize: 10,
                                        fontColor: '#000'
                                    }
                                },
                                layout: {
                                    padding: {
                                        left: 0,
                                        right: 0,
                                        top: 20,
                                        bottom: 20
                                    }
                                }


                            }

                        });


                    }//else end

               });
            }

        });

    </script>
    <script>
        $.list();
        $('#monthly').change(function(){
            $.list();
        });

        $('#month-year').change(function(){
            $.list();
        });

    </script>

@endsection

