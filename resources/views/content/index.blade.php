@extends('master_layout.master')
@section('content')
   
    <button type="button" class="btn btn-success shadow-lg " data-toggle="modal" data-target="#exampleModal">
        <i class="fas fa-plus-circle"></i> Add item
    </button>
    
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{--  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add item</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>  --}}
                <div class="modal-body">
                    <form class="add_record" method="post" action="/expenses">
                        @csrf
                        @php
                            $current_date = Carbon\Carbon::now()->toDateString();
                        @endphp

                        <div class="form-group">
                            <label for="">Date</label>
                            <input name="date" class="form-control datepicker" id="datepicker" value="{{$current_date }}" date-date-format="yy-mm-dd" required>

                            {{--  <small id="item_small" class="form-text text-muted">Select the date of expenses.</small>  --}}
                        </div>
                        <div class="form-group">
                            <label for="">Category</label>
                            <select class="form-control" name="category" value="{{ old('category') }}" required autofocus>
                                <option selected disabled>Select Category</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                                @endforeach

                            </select>
                            @if ($errors->has('category'))
                                <small id="item_small" class="text-danger">{{ $errors->first('category') }}</small>

                            @endif

                        </div>

                        <div class="form-group">
                            <label for="">Item</label>
                            <input type="text" class="form-control" id="item" name="item" value="{{ old('item') }}" autofocus>
                            @if ($errors->has('item'))
                                <small id="item_small" class="text-danger">{{ $errors->first('item') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Details</label>
                            <input type="text" class="form-control" id="details" name="details" value="{{ old('details') }}" autofocus>
                            @if ($errors->has('details'))
                                <small id="item_small" class="text-danger">{{ $errors->first('details') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Price</label>
                            <input type="number" class="form-control" id="price" name="price" value="{{ old('price') }}" autofocus>
                            @if ($errors->has('price'))
                                <small id="item_small" class="text-danger">{{ $errors->first('price') }}</small>
                            @endif
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <br><br>
    <table class="table shadow-lg">
        <thead>
            <th>Date</th>
            <th>Category</th>
            <th>Item</th>
            <th>Details</th>
            <th>Amount</th>
        </thead>
        <tbody>
            @foreach ($expenses as $expense)
                <tr class=" " >
                    <td><a href="{{ route('expenses.show',$expense->id)}}">{{ $expense->date }}</a></td>
                    <td><a href="{{ route('expenses.show',$expense->id)}}">{{ $expense->category->title }}</td>
                    <td><a href="{{ route('expenses.show',$expense->id)}}">{{ $expense->item }}</td>
                    <td><a href="{{ route('expenses.show',$expense->id)}}">{{ $expense->details }}</td>
                    <td><a href="{{ route('expenses.show',$expense->id)}}">{{ $expense->price }}</td>

                </tr>
            @endforeach
        </tbody>
    </table>
    <br><br>
    {{ $expenses->links() }}
@endsection

@section('sidebar')
    <canvas id="myChart" width="400" height="250"></canvas>
@endsection

@section('script')
    <script>
      
        // $("#table").delegate("tr", "click", function(){
        //     alert("Click!");
        // });
    </script>
   
    <script >
        @if(count($errors) > 0)
            $('#exampleModal').modal('show')                  
        @endif
    </script>
    <script>
        var data = @json($charts);
        var price = [];
        var title = [];
        var total = 0;
        for(var i = 0;  i < data.length; i++) {
            title.push([data[i]['category']['title']]);
            price.push([data[i]['price']]);
            total += data[i]['price'] << 0; //convert the string elements to numerics using '<< 0'

        }
        

        var ctx = document.getElementById("myChart");
        ctx.width = 500;
        ctx.height = 400;
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: title,
                datasets: [{
                    data: price,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],

                    borderWidth: 1,
                    fontColor: '#666'


                }]
            },
            options: {

                title: {
                    display: true,
                    text: 'Total Expenses '+total,
                    fontSize: 25,
                    padding: 20


                },
                tooltips:{
                    enable: true,
                    bodyFontSize: 20
                },
                legend:{
                    display: true,
                    position: 'right',
                    labels:{
                        fontSize: 10,
                        fontColor: '#000'
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 20,
                        bottom: 20
                    }
                }


            }

        });
    </script>
    {{--  <script type="text/javascript">
         $('.add_record').on('submit', function() {
           
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: '/expenses',
                data:  new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData:false,
                success: function(data) {
                  
                       
                    $('#table').append("<tr class='item" + data.id + "'><td>" + data.date + "</td><td>" + data.item + "</td><td>" + data.details + "</td><td>" + data.price + "</td>");
                      
                 
                },   
                
            });
        });
       
       
    </script>  --}}
@endsection

