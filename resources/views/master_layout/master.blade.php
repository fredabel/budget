<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="{{ asset('css/datepicker.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css">
    {{--  <link href="{{ asset('font-awesome/css/all.css') }}" rel="stylesheet">  --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <title>Budget</title>
</head>
<body>
    <div class="main-page" id="app">

        <header>
            @include('master_layout.navbar')

        </header>
        <section id="income-box">
            @include('content.income')
        </section>
        <section id="sidebar-left">
            @include('master_layout.sidebar-left')
        </section>

        <main>
            @yield('yearly')

            @yield('content')
        </main>
        <section id="sidebar-right">
            @yield('sidebar')
        </section>


        <footer>
            @include('master_layout.footer')
        </footer>

    </div>

    <script src="{{ asset('js/app.js') }}"></script>

    <script type="text/javascript" src="{{ URL::to('js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/Chartjs/Chart.js') }}"></script>
    {{--  <script defer src="{{ asset('font-awesome/js/all.js') }}"></script>   --}}

    @yield('script')
    <script>

        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',

        });

    </script>

</body>
</html>
