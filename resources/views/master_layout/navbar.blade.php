<div id="logo">
    
    <a href="" id="side"><img src="{{ asset('images/logo.png') }}" alt="Logo"></a>
    
</div>
<div id="menu">
    <ul>
        <li><a href="">Dashboard</a></li>
        <li><a href="/expenses">Budget</a></li>
        
        
    </ul>
</div>

<div id="sign">
    <ul>
        <li class="nav-item dropdown">
                <a id="" >
                   {{ Auth::user()->name }}
                </a>

        </li>
        <li class="nav-item">
            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
                
        </li>
        
    </ul>   
</div> 

