{{--  <ul>

    <li><a href="/expenses">All</a></li>
    <li><a href="/daily">Daily</a></li>
    <li><a href="/weekly">Weekly</a></li>
    <li><a href="/monthly">Monthly</a></li>
    <li><a href="/yearly">Yearly</a></li>

</ul>  --}}



    <ul class="nav nav-pills">
		<li class="nav-item header ">
			<a class="" href=""> Menu Header</a>
			<hr>
		</li>
        <li class="nav-item ">
			<a class="" href="{{ route('category.index') }}"><i class="fas fa-grip-horizontal "></i> Category</a>
        </li>
        <li class="nav-item ">
				<a class=" " href="{{ route('expenses.index') }}"><i class="fas fa-grip-horizontal "></i> All</a>
            {{--  <a class="nav-link active" href="#">Active</a>  --}}
        </li>
        <li class="nav-item " >
            <a class="" href="/daily"> <i class="fas fa-grip-horizontal "></i>Daily</a>
        </li>
        <li class="nav-item">
            <a class=" " href="/weekly"><i class="fas fa-grip-horizontal "></i>Weekly</a>
        </li>
        <li class="nav-item">
            <a class=" " href="/monthly"><i class="fas fa-grip-horizontal "></i>Monthly</a>
        </li>
        <li class="nav-item">
            <a class=" " href="/yearly"><i class="fas fa-grip-horizontal "></i>Yearly</a>
        </li>
    </ul>

