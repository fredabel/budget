<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// login google
// Route::get('/redirect', 'Auth\LoginController@redirectToProvider');
// Route::get('/callback', 'Auth\LoginController@handleProviderCallback');

//login facebook
Route::get('/redirect', 'Auth\SocialAuthFacebookController@redirect');
Route::get('/callback', 'Auth\SocialAuthFacebookController@callback');

// Route::post('login', 'API\UserController@login');
// Route::post('register', 'API\UserController@register');

// Route::group(['middleware' => 'auth:api'], function(){
//     Route::post('details', 'API\UserController@details');
// });

Route::get('/', function(){
    return view ('welcome');
    if(\Auth::check()){
    	return \Redirect::to('/home');

    }
    return \Redirect::to('/login');


});


/* Expenses */
Route::resource('expenses', 'ExpensesController');


/* Category */
Route::resource('category', 'CategoryController');



/* Daily */

Route::get('daily','ByDateController@daily');
Route::post('daily','ByDateController@daily_filter');

/* Weekly */
Route::get('weekly','ByDateController@weekly');
Route::post('weekly','ByDateController@weekly_filter');

/* Monthly */
Route::get('monthly','ByDateController@monthly');
Route::post('monthly','ByDateController@monthly_filter');

/* Yearly */
Route::get('yearly','ByDateController@yearly');
Route::post('yearly','ByDateController@yearly_filter');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

