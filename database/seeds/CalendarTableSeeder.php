<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CalendarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($iM =1;$iM<=12;$iM++){
            $month = date("M", strtotime("$iM/12/10"));
                DB::table('calendars')->insert([
                'month' =>$month,
                'year' => '2019',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
           
            ]);
        }
        for($iM =1;$iM<=12;$iM++){
            $month = date("M", strtotime("$iM/12/10"));
                DB::table('calendars')->insert([
                'month' =>$month,
                'year' => '2020',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
           
            ]);
        }
        
    }
}
