<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ExpensesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('expenses')->insert([
            
            'date'  => Carbon::now(),
            'category_id' => '1',
            'item' => 'Rice',
            'details' => 'Breakfast',
            'price' => 50,
            'week'  =>  date('W'),
            'status'    => 'active',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('expenses')->insert([
            'date'  => Carbon::now(),
            'category_id' => '2',
            'item' => 'House',
            'details' => 'Rent',
            'price' => 12000,
            'week'  =>  date('W'),
            'status'    => 'active',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);
        DB::table('expenses')->insert([
            'date'  => Carbon::now(),
            'category_id' => '3',
            'item' => 'Electricity bill',
            'details' => 'Electric',
            'price' => 1000,
            'week'  =>  date('W'),
            'status'    => 'active',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);
        DB::table('expenses')->insert([
            'date'  => Carbon::now(),
            'category_id' => '4',
            'item' => 'WAter Bill',
            'details' => 'Water ',
            'price' => 500,
            'week'  =>  date('W'),
            'status'    => 'active',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);
        DB::table('expenses')->insert([
            'date'  => Carbon::now(),
            'category_id' => '5',
            'item' => 'Transpo',
            'details' => 'Allowance',
            'price' => 150,
            'week'  =>  date('W'),
            'status'    => 'active',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);
        DB::table('expenses')->insert([
            'date'  => Carbon::now(),
            'category_id' => '6',
            'item' => 'Refrigerator',
            'details' => 'It is cool',
            'price' => 3000,
            'week'  =>  date('W'),
            'status'    => 'active',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);
        DB::table('expenses')->insert([
            'date'  => Carbon::now(),
            'category_id' => '1',
            'item' => 'Dinner',
            'details' => 'Jollibee',
            'price' => 500,
            'week'  =>  date('W'),
            'status'    => 'active',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);
    }
}
