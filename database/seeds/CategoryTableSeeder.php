<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        DB::table('categories')->insert([
            'title' => 'Food',
            'description' => 'Lorem ipsum dolor sit.',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);
        
        DB::table('categories')->insert([
            'title' => 'Rent',
            'description' => 'Lorem ipsum dolor sit.',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('categories')->insert([
            'title' => 'Electricity',
            'description' => 'Lorem ipsum dolor sit.',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('categories')->insert([
            'title' => 'Water',
            'description' => 'Lorem ipsum dolor sit.',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);
        DB::table('categories')->insert([
            'title' => 'Allowance',
            'description' => 'Lorem ipsum dolor sit.',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);
        DB::table('categories')->insert([
            'title' => 'Misc',
            'description' => 'Lorem ipsum dolor sit.',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);
      
    }
}
